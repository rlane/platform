var Ground = function() {
	this.y = 500;
	this.friction = -5;
	window.ground = this;
};

Ground.prototype = {
	draw: function() {		
		for (var i = 0; i < window.viewport.getWidth(); i++) {
			window.viewport.setPixel(i, this.y, [0, 0, 0]);
		}
	},
	getY: function() {
		return this.y;
	}
};