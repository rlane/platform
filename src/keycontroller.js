KeyController = function() {
	window.keycontroller = this;
	this.downs = {};
	this.ups = {};
	document.onkeydown = function(event) {
		var handler = window.keycontroller.getDown(event.keyCode);
		if (handler) {
			console.info(event.keyCode);
			handler.call(window.player, event);
		}
	},
	document.onkeyup = function(event) {
		var handler = window.keycontroller.getUp(event.keyCode);
		if (handler) {
			console.info(event.keyCode);
			handler.call(window.player, event);
		}
	}
};

KeyController.prototype = {
	getDown: function(key) {
		return this.downs[key];
	},
	getUp: function(key) {
		return this.ups[key];
	},
	setAction: function(key, down, up) {
		this.downs[key] = down;
		this.ups[key] = up;
	}
};