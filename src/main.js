require(["player", "ground", "viewport", "keycontroller", "movingobjectscontroller"], function() {
	var ctx = document.getElementById("canvas-main").getContext('2d');
	window.ctx = ctx;
	var w = ctx.canvas.width = window.innerWidth;
	var h = ctx.canvas.height = window.innerHeight;
	
	var viewport = new Viewport(ctx, w, h);
	window.viewport = viewport;
	
	var ground = new Ground();
	ground.draw();
	
	var player = new Player(100, ground.getY());
	player.draw(100, ground.getY());
	viewport.draw();
	
	new MovingObjectsController().addObject(window.player);
	
	new KeyController().setAction(68, player.beginMoveRight);
	window.keycontroller.setAction(65, player.beginMoveLeft);
	setInterval(function(){
		window.movingObjectsController.processFrame();
		window.viewport.draw();
		console.info(window.player.x);
	}, movingObjectsController.getInterval());
});