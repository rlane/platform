MovingObjectsController = function() {
	this.interval = 10;
	this.objects = [];
	window.movingObjectsController = this;
};

MovingObjectsController.prototype = {
	addObject: function(obj) {
		this.objects.push(obj);
	},
	getInterval: function() {
		return this.interval;
	},
	processFrame: function() {
		// todo movable "interface"
		for (var i = 0; i < this.objects.length; i++) {
			var obj = this.objects[i];
			if (obj.velocity != 0) {
				obj.x += obj.velocity * (this.interval / 1000);
				
				if (obj.velocity < 0) {
					obj.velocity += obj.friction * (this.interval / 1000);
				} else {
					obj.velocity -= obj.friction * (this.interval / 1000);
				}				
				
				if (Math.abs(obj.velocity) < 0.1) {
					obj.velocity = 0;
				}
			}			
		}
	}
};