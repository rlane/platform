var Player = function(x, y) {
	this.x = x;
	this.y = y;
	this.h = 32;
	this.w = 32;
	this.max_velocity = 300;
	this.velocity = 0;
	this.friction = 1500;
	window.player = this;
};

Player.prototype = {
	draw: function() {
		var realx = Math.floor(this.x);
		var realy = Math.floor(this.y);
		for (var j = 0; j < this.h; j++) {
			for (var i = 0; i < this.w; i++) {
				window.viewport.setPixel(realx - this.w / 2 + i, realy - this.h + j, [0, 255, 0]);
			}
		}		
	},
	beginMoveLeft: function() {
		this.velocity = - this.max_velocity;
	},
	beginMoveRight: function() {
		this.velocity = this.max_velocity;
	}
};