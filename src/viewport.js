var Viewport = function(ctx, w, h) {
	this.ctx = ctx;
	this.w = w;
	this.h = h;
	this.imageData = ctx.createImageData(w, h);
};

Viewport.prototype = {
	draw: function() {
		this.imageData = this.ctx.createImageData(this.w, this.h);
		window.ground.draw();
		window.player.draw();
		this.ctx.putImageData(this.imageData, 0, 0);
	},
	getHeight: function() {
		return this.h;
	},
	getWidth: function() {
		return this.w;
	},
	setPixel: function(x, y, color) {
		var index = (x + y * this.w) * 4;
		this.imageData.data[index + 0] = color[0];
		this.imageData.data[index + 1] = color[1];
		this.imageData.data[index + 2] = color[2];
		this.imageData.data[index + 3] = 255;
	}
};